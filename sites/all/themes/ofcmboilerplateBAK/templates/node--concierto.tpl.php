<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
  <div class="node-inner">
		<?php if (!$page): ?>
			<header>
				<h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
			</header>
		<?php endif; ?>
		<?php if ($page) { ?>
			<div class="content">
					<?php 
						// Get field arrays
						$fechas = field_get_items('node', $node, 'field_fechas');
						$sedes = field_get_items('node', $node, 'field_sedeentidad');
						$boletos = field_get_items('node', $node, 'field_boletos');
					?>
				<ul class="concertdates clearfix">
					<?php 
						// Get data row
						foreach ($fechas as $key=>$value) {
							$col1 = field_view_value('node', $node, 'field_fechas', $fechas[$key], array('settings' => array ('format_type' => 'short')));
							$col3 = field_view_value('node', $node, 'field_boletos', $boletos[$key]);
						// Print table row
						?>
					<li class="dateitem clearfix">
						<strong><?php print drupal_render($col1);	?></strong><br/>
						<?php if ($page) print '<span>Sala Silvestre Revueltas</span><br/>';	?>
						<a href="<?php print drupal_render($col3);?>">Compra tus boletos</a> 
					</li>
						<?php
						}
					 ?>
				</ul>
			</div>
			<?php 
				// We hide the comments and links now so that we can render them later.
				hide($content['comments']);
				hide($content['links']);
				print render($content);
			} else { ?>
			<div class="content clearfix">
				<?php 
					// Get field arrays
					$fechas = field_get_items('node', $node, 'field_fechas');
					$boletos = field_get_items('node', $node, 'field_boletos');
					print render($content['field_ilustracion']);
				 ?>
				<ul class="concertdates clearfix">
					<?php 
						// We hide the comments and links now so that we can render them later.
						// Get data row
						foreach ($fechas as $key=>$value) {
							$col1 = field_view_value('node', $node, 'field_fechas', $fechas[$key], array('settings' => array ('format_type' => 'fullflat')));
							$col3 = field_view_value('node', $node, 'field_boletos', $boletos[$key]);
						?>
						<li class="dateitem">
							<div class="date">
								<strong><?php print drupal_render($col1);	?></strong>
							</div>
							<div class="boletos">
								<a href="<?php print drupal_render($col3);?>">Boletos</a> 
							</div>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php 
		} ?>

    <?php if (!empty($content['links']['terms']) || !empty($content['links'])): ?>
      <footer>
      <?php if (!empty($content['links']['terms'])): ?>
        <div class="terms"><?php print render($content['links']['terms']); ?></div>
      <?php endif;?>
      
      <?php if (!empty($content['links'])): ?>
        <div class="links"><?php print render($content['links']); ?></div>
      <?php endif; ?>
      </footer>
    <?php endif; ?>
  </div> <!-- /node-inner -->
</article> <!-- /node-->
<?php print render($content['comments']); ?>
